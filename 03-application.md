# Rannikkokalastus


## Tiedonkeruu

- Otoksen muodostaminen
- Tallennus
- Raakadata
  
  
## SAS-Ohjelmat

- 01-rk-surveyreg
- 02-perusdata
- 03-laskenta
- 04-cv_laskenta
- 05-kuluosuudet


### rk-surveyreg {-}

#### PROC SURVEYREG {-}

Source: http://www.math.wpi.edu/saspdf/stat/chap62.pdf

The PROC SURVEYREG statement invokes the procedure. You can specify the following options in the PROC SURVEYREG statement.

##### Data {-}

specifies the SAS data set to be analyzed by PROC SURVEYREG. If you omit the DATA= option, the procedure uses the most recently created SAS data set.

##### Total {-}

specifies the total number of primary sampling units in the study population as a positive value, or specifies an input data set that contains the stratum population totals. The procedure uses this information to compute a finite population correction for variance estimation. For a nonstratified  sample design, or for a stratified sample design with the same population total in all strata, you should specify a positive value for the TOTAL= option. If your sample design is stratified with different population totals in the strata, then you should name a SAS data set that contains the stratification variables and the population totals. See the section 'Specification of Population Totals and Sampling Rates' on page 3234 for details. If you do not specify the TOTAL= option or the RATE= option, then the variance estimation does not include a finite population correction.

#### Strata {-}

The STRATA statement specifies variables that form the strata in a stratified sample design. The combinations of categories of STRATA variables define the strata in the sample. If your sample design has stratification at multiple stages, you should identify only the first-stage strata in the STRATA statement. See the section 'Specification of Population Totals and Sampling Rates' on page 3234 for more information. The STRATA variables are one or more variables in the DATA= input data set. These variables can be either character or numeric. The procedure  uses only the first 16 characters of the value of a character variable. The formatted values of the STRATA variables determine the levels. Thus, you can use formats to group values into levels. Refer to the discussion of the FORMAT procedure in the SAS Procedures Guide. You can use multiple STRATA statements to specify stratum variables.

##### List {-}

displays a 'Stratum Information' table, which includes values of the STRATA variables, and the number of observations, number of clusters, population total, and sampling rate for each stratum. This table also displays stratum collapse information.

#### Model {-}

The MODEL statement specifies the dependent (response) variable and the independent (regressor) variables or effects. Each term in a MODEL statement, called an effect, is a variable or a combination of variables. You can specify an effect with avariable name or with a special notation using variable names and operators. For more information on how to specify an effect, see the section 'Specification of Effects' on page 1517 in Chapter 30, 'The GLM Procedure.' The dependent variable must be numeric. Only one MODEL statement is allowed for each PROC SURVEYREG statement. If you specify more than one MODEL statement, the procedure uses the first model and ignores the rest.

##### Noint {-}

omits the intercept from the model.

##### Solution {-}

displays a solution to the normal equations, which are the parameter estimates. The SOLUTION option is useful only when you use a CLASS statement. If you do not specify a CLASS statement, PROC SURVEYREG displays parameter estimates by default. But if you specify a CLASS statement, PROC SURVEYREG does not display parameter estimates unless you also specify the SOLUTION option.

#### Weight {-}

The WEIGHT statement specifies the variable that contains the sampling weights. This variable must be numeric. If you do not specify a WEIGHT statement, PROC SURVEYREG assigns all observations a weight of 1. Sampling weights must be positive numbers. If an observation has a weight that is nonpositive or missing, then the procedure omits that observation from the analysis. If you specify more than one WEIGHT statement, the procedure uses only the first WEIGHT statement and ignores the rest.

#### Estimate {-}

You can use an ESTIMATE statement to estimate a linear function of the regression parameters by multiplying a row vector $L$ by the parameter estimate vector $B$.

Each term in the MODEL statement, called an *effect*, is a variable or a combination of variables. You can specify an effect with a variable name or with a special notation using variable names and operators. For more details on how to specify an effect, see the section 'Specification of Effects' on page 1517 in Chapter 30, 'The GLM Procedure'.

PROC SURVEYREG checks the linear function for estimability. (See the SINGULAR= option described on page 3231).

The procedure displays the estimate $L\hat{\beta}$ along with its standard error and *t* test. If you specify the CLPARM option in the MODEL statement, PROC SURVEYREG also displays confidence limits for the linear function. By default, the degrees of freedom for the t-test equals the number of clusters (or the number of observations if there is no CLUSTER statement) minus the number of strata. Alternatively, you can specify the degrees of freedom with the DF= option in the MODEL statement.

You can specify any number of ESTIMATE statements, but they must appear after the MODEL statement.

In the ESTIMATE statement

- label identifies the linear function $L$ in the output. A label is required for every function specified. Labels must be enclosedin single quotes.

- effect identifies an effect that appears in the MODEL statement. You can use the INTERCEPT keyword as an effect when an intercept is fitted in the model. You do not need to include all effects that are in the MODEL statement.

- values are constants that are elements of the vector $L$ associated with the effect. For example, the following code forms an estimate that is the difference between the parameters estimated for the first and second levels of the CLASS variable $A$.

`estimate ’A1 vs A2’ A  1 -1;`



##### E {-}

displays the entire coefficient vector $L$.


### Perusdata {-}

- Haetaan kirjanpitokyselyn vuosiaineisto
- Yhdistetään kirjanpitoaineisto kaupallisen kalastuksen kehikon tietoihin
- Lasketaan samalla 'hyht_per_kalamyynti' ja 'mtuot_per_tuotyht'
- Putsataan aineistoa ja poistetaan poikkeavia havaintoja
- Yhdistetään hyljesietokorvaukset aineistoon
- Summataan hyljesietokorvaukset y-tunnuksittain
- Muokataan dataa sopivammaksi laskentaa varten
- Lasketaan tuotot yhteensä ilman hyljesietokorvausta
- Lasketaan muut_tuotot ja nettotuotot todellisilla hsk:lla

### Laskenta {-}

Tämä ohjelma korvaa aikaisempien vuosien taltoht-koodin. Ohjelma koostuu kolmesta eri osiosta, jotka tässä on yhdistetty samaan koodiin.

#### Liikevaihdon estimointi (A1) {-}

- Yhdistetään hsk kehikkoon y-tunnuksella = kehikko_hsk
- Yhdistetään kehikko_hsk kirjanpitoaineistoon
- Rajataan luokitus_eu = 'PG VL0010';
- Tehdään ositteet = pyydys- ja arvoluokka
- Tallennetaan hylje_temp10 Tilastokeskuksen kehikon laskentaa varten
- Tallennetaan hylje_temp20x G:lle vuosiraporttia varten
- Muodostetaan arvomatriisi
- Muodostetaan Totals-taulu estimointia varten
- Estimointi: %survey&n (selitettava=lvaihto, apumja=Arvo)
- Yhdistetään pienimmän liikevaihdon osite seuraavaan luokkaan
- Laitetaan liikevaihdolle lasketut estimaatit (summat) makromuuttujiin
- Tehdään uudet painot vanhojen ryhmien mukaan lvaihto-surveyregiä varten (A2)
- Yhdistetään painot estimaattitauluun


#### Tuottojen ja kustannusten estimointi (A2) {-}

- Tehdään datamatriisi liikevaihdoista regressointia varten
- Muodostetaan Totals-taulu estimointia varten
- Estimointi: tuotyht_hsk
- Estimointi: kustannuksetyht
- Estimointi: nettovoitto_hsk
- Estimointi: laina
- Estimointi: fte


#### Suhdelaskenta & rivitason estimaatit (A3) {-}

- Yhdistetään ositteet jos tarvetta (ks. A1)
- Lasketaan suhdemuuttujien prosenttiosuudet liikevaihtoluokittain
- Luodaan paino korkojen ja poistojen painotukselle cv-laskentaa varten
- Lisätään rivitason estimaatit puuttuville: Luodaan makro ja tuodaan sen avulla puuttuville riveille regressoidut arvot, säilyttäen alkuperäiset ennallaan
- Muutetaan vielä luokitusta siten, että nollaluokat ovat ykkösiä
- Korjataan lvluokka liikevaihdosta riippuvaiseksi (eikä arvosta)
- Luodaan taulu, jossa on sekä tulo- ja kuluerien prosenttiosuudet että rivitason estimaatit
- Lasketaan tulo- ja kuluerien estimaatit suhdelaskennalla
- Yhdistetään alkuperäiset tiedot regressoitujen tilalle, jos löytyy
- Muokataan julkaisukelpoinen taulu taloustohtoriin


## Julkaisu

Rannikkokalastuksen tulokset julkaistaan Taloustohtorissa laskennan valmistuttua. Julkaisulle ei ole määritetty tarkkaa ajankohtaa, mutta yleisesti tavoitteena on saada tulokset julki lokakuun aikana. Ajallisesti voisi olla loogista odottaa, että Kalatalouden toimialakatsaus on saatu julkaistua ensin, koska rannikkokalastus aloittaa uuden 'tilastovuoden' ja on siis ensimmäinen osa seuraavan vuoden toimialakatsausta. Rannikkokalastuksesta ei tehdä omaa tiedotetta.

Vuodesta 2020 lähtien Taloustohtoriin ei enää viedä rannikkokalastuksen laskentakoodia. Aiemmin on toimittu niin, että Taloustohtoriin on viety ainoastaan raaka-aineisto sekä SAS-ohjelma, joka on tuottanut tulokset 'lennossa' ja summannut tulokset käyttäjän valitsemien luokitusten mukaisesti. Tämä menettely ei kuitenkaan ole viime vuosina tuonut oikeastaan mitään lisäarvoa palveluun. Sen takia päätettiin luopua laskennasta ja viedä Taloustohtoriin rannikkokalastuksen rivitason tiedot, joista tulokset luokitusten mukaisesti lasketaan. Tämä helpottaa hieman vuosittaisia päivityksiä eikä enää tarvitse olla kahta rinnakkaista versiota SAS-ohjelmasta. Lisäksi on ajateltu, että samainen rivitason tulostaulu vietäisiin Postgres-tietokantaan, jolloin on loogista, että Taloustohtorissa käytetään samaa data-taulua. 