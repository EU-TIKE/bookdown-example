# Merialueen kaupallisen kalastuksen kehikko {#kehikko}

Kaupallisen kalastuksen kehikon muodostusta on viime vuosina pyritty päivittämään, koska monet työvaiheet eivät olleet enää olennaisia kehikon kannalta. EU-tiedonkeruu projektia varten on muodostettu ns. metier-taulut, joiden pohjalta kehikko nykyisin muodostetaan. Metier-taulut sisältävät suurimman osan taloudellisellakin puolella käytettävistä luokituksista (pl. rk-luokitus) ja niiden käyttäminen lähtötiedostoina mahdollistaa yhtenäisen ja tehokkaamman kehikon muodostuksen. Myös saaliin arvo on laskettu valmiiksi saalisilmoituskohtaisesti. Näin ollen hintatietoja ei tarvitse erikseen päivittää ja voidaan olla varmoja, että saaliin arvo täsmää saalistietoihin.  


Metier-taulut ovat lähes identtisiä vuodesta toiseen, joten uusi kehikkokoodi toimii (pitkälti) vuodesta toiseen. Koodi ei ole enää sidottu tiettyyn vuoteen, jolloin ylläpito ja jatkokehittäminen on vaivattomampaa. Toki se tekee myös koodin herkemmäksi mm. lähtöaineiston muutoksille eli on tärkeää, että metier-taulujen sisältö sekä rakenne pysyisivät vuodesta toiseen mahdollisimman muuttumattomana. Uuden koodin ajatuksena on, että minkä tahansa vuoden kehikko voidaan muodostaa helposti käyttämällä samaa ohjelmaa hyödyntämällä makromuuttujia %vuosi sekä %id-tunnusta.  


Metier-taulujen käyttö ei sinäänsä vaikuta kehikon lopputulokseen millään tavalla, mutta kehikkoon tehty myös muita päivityksiä ja korjauksia. Tässä on lyhyesti listattu tärkeimmät muutokset:  


- Vanha kehikko on summattu ensin aluksen perusteella ja sen jälkeen asiakastunnuksittain. On kuitenkin tapauksia, joissa samaa alusta käyttää useampi eri toimija (astunnus), jolloin taloudellisesta näkökulmasta saalis voi kohdistua väärälle toimijalle. Uudessa kehikossa summaus tehdään saalisilmoituksista suoraan asiakastunnuksille, jolloin saalis ja saaliin arvo kohdistuvat oikealle asiakastunnukselle. Tämä vaikuttaa myös jonkin verran kehikon toimijoiden lukumäärään - lähinnä rannikkokalastuksen osalta.
- Myös ilman alusta (jään alta) pyydetyt saaliit ja niitä koskevat saalisilmoitukset tulevat nyt huomioiduksi saaliissa ja saaliin arvossa sekä ft-luokituksessa joka valitaan suurimman saaliin arvon mukaisesti kaikkien kalastustekniikoiden joukosta.
- Paritroolarit ilmoittavat saaliin usein kokonaisuudessaan yhdelle alukselle. Uudessa kehikossa paritroolarien saalis jaetaan 50/50 molemmille pareille.
- TM VL1012 on nykyisin oma alussegmentti eikä sitä enää klusteroida luokkaan TM VL1218
- Toimijatunnus on nykyisin käytettävä tunnistemuuttuja asiakastunnuksen sijaan. Tämä muutos ei vaikuta kehikon lukumäärään. Vanhoille vuosille voidaan helposti käyttää asiakastunnusta vaihtamalla %id-muuttujaa.
- Saalisilmoitukseen tallentuu aina ilmoituksen tehneen toimijan tai henkilön toimija/asiakastunnus. Tämä tarkoittaa sitä, että ilmoituksessa (sekä metier-taulussa näkyvä) tunnus voi olla kaupallisen kalastajan oma, kalastajan edustajan tai valtuutetun allekirjoittajan. Tämä on tärkeää tiedostaa etenkin KAKE:n kalastajatietojayhdistäessä. Uudessa kehikkokoodissa KAKE-tietoja yhdistetään sekä kalastajien oman että edustajan toimijatunnukse avulla. Näin ollen tietoja pitäisi löytyä aiempaa paremmin.  



	- [x] Paritroolareiden saalisjako
	- [x] Saalis ilman alusta
	- [x] ft ja vl astunnus-tasolla
	- [x] Saalistiedot lajeittain
	- [x] luokitus_eu_p / luokitus_eu_&vuosi
	- [x] Alus (ja gear) id-tasolle suurimman hyhtin mukaan
	- [x] kapasiteettitiedot (astunnus)
	- [x] kapasiteettitiedot (alus)
	- [ ] Y-tunnus & kalastajaryhmä
	- [ ] kunta (ja muut alueelliset)
	- [ ] pyyntipäivät
	- [ ] klusterointi: TM VL1012 omaksi luokaksi?
	- [ ] otetaanko pt-saalisjako huomioon luokituksessa?
	- 
	
**Koodit:**

- 0-data-import
  - määrittää projektikansion sekä alikansiot
  - hakee metier-taulut projektikansioon
  - määrittää makromuuttujat lajeille ja lajikohtasille arvoille
  - määrittää makromuuttujana vuoden sekä id:n
- 1-saaliin-arvo
  - jakaa paritroolarien saaliit puoliksi
  - yhdistää saalisilmoitukset ja summaa &id-tasolle
- 2-rk-luokitus
  - määrittää rk-luokituksen saaliin arvon mukaan &id-tasolle
- 3-ft-&id
  - määrittää jokaiselle &id-tasolle suurimman arvon mukaisen ft:n
  - luokitus_eu_p / luokitus_eu
- 4-kalastajatiedot
  - hakee KAKE:n kalastajatiedot projektikansiosta
  - lytunnus kalastajaryhma kunta_knro
  - yhdistää aikaisempien koodien taulut &id-tasolle
- 5-paritroolarit
  - määrittää paritroolin alukselle suurimman arvon mukaan
- run-all
  - määrittää makromuuttujat ja ajaa kaikki koodit yksitellen  



## Miten jatketaan?

- PIM-laskennassa tuotettuja arvoja ei kohdisteta rivitasolle vaan ne raportoidaan ainoastaan alus-segmenteittäin: täytyy silti täsmätä alustietoihin